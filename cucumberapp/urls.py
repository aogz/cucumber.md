from django.conf.urls import patterns, include, url
from cucumberapp.views import *


urlpatterns = patterns('',
    # Examples:
    url(r'^$', home, name='home'),
    url(r'^category/(?P<category_id>\d+)/$', catalogue, name='catalogue'),
    url(r'^brand/(?P<brand_id>\d+)/$', brand, name='brand'),
    url(r'^product/(?P<product_id>\d+)/$', product, name='product'),
    url(r'^page/(?P<page_url>[-\w]+)/$', page, name='page'),
    url(r'^search/$', search, name='url'),

    # url(r'^contact/$', ContactView.as_view(), name='contact'),
)
