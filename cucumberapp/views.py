from django.http import HttpResponse, HttpResponseRedirect, Http404
from django.shortcuts import redirect, render, get_object_or_404
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.views.decorators.csrf import csrf_exempt
from .models import *

from django.template import RequestContext


def handler404(request):
    response = render(request, '404.html', {})
    response.status_code = 404
    return response


def handler500(request):
    response = render(request, '500.html', {})
    response.status_code = 500
    return response


# Create your views here.
def home(request):

    # Vars for items on main page
    last_products = Item.objects.all().filter(active=True).order_by('date_added').reverse()[:6]
    recommended_products = Item.objects.all().filter(active=True).order_by('date_added').reverse()[:3]
    recommended_products2 = Item.objects.all().filter(active=True).order_by('date_added').reverse()[3:6]

    # Quick Order Form
    if request.method == 'POST':
        name = request.POST.get("name", "")
        phone = request.POST.get("phone", "")
        item = request.POST.get("item", "")
        more = request.POST.get("more", "")
        order = Order.objects.create(name=name, phone=phone, item=item, more=more)
        order.save()

        return render(request, 'index.html', locals())

    return render(request, 'index.html', locals())


def catalogue(request, category_id):
    category = Category.objects.get(id=category_id)
    products = Item.objects.all().filter(category=category).filter(active=True)

    if request.method == 'POST':
        name = request.POST.get("name", "")
        phone = request.POST.get("phone", "")
        item = request.POST.get("item", "")
        more = request.POST.get("more", "")
        order = Order.objects.create(name=name, phone=phone, item=item, more=more)
        order.save()
        return render(request, 'index.html', locals())

    return render(request, 'shop.html', locals())


def product(request, product_id):
    product = Item.objects.get(id=int(product_id))
    product_images = ItemImage.objects.all().filter(item=product)[1:]

    if request.method == 'POST':
        name = request.POST.get("name", "")
        phone = request.POST.get("phone", "")
        item = request.POST.get("item", "")
        more = request.POST.get("more", "")
        order = Order.objects.create(name=name, phone=phone, item=item, more=more)
        order.save()

        return render(request, 'product.html', locals())

    return render(request, 'product.html', locals())


def brand(request, brand_id):
    brand = Brand.objects.get(id=brand_id)
    products = Item.objects.all().filter(brand=brand).filter(active=True)

    if request.method == 'POST':
        name = request.POST.get("name", "")
        phone = request.POST.get("phone", "")
        item = request.POST.get("item", "")
        more = request.POST.get("more", "")
        order = Order.objects.create(name=name, phone=phone, item=item, more=more)
        order.save()
        return render(request, 'index.html', locals())

    return render(request, 'shop.html', locals())


def page(request, page_url):
    page = Page.objects.get(url=page_url)
    return render(request, 'shop.html', locals())


def search(request):
    if 'q' in request.GET and request.GET['q']:
        q = request.GET['q']
        products = Item.objects.filter(title__icontains=q)

    return render(request, 'shop.html', locals())




