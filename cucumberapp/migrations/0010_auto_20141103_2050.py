# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('cucumberapp', '0009_auto_20141103_1601'),
    ]

    operations = [
        migrations.CreateModel(
            name='Brand',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=255)),
                ('description', models.TextField()),
                ('logo', models.ImageField(upload_to=b'brands', verbose_name='\u0418\u0437\u043e\u0431\u0440\u0430\u0436\u0435\u043d\u0438\u0435')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.RenameField(
            model_name='item',
            old_name='operating_sistem',
            new_name='operating_system',
        ),
        migrations.AddField(
            model_name='item',
            name='brand',
            field=models.ForeignKey(blank=True, to='cucumberapp.Brand', null=True),
            preserve_default=True,
        ),
    ]
