# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('cucumberapp', '0015_item_active'),
    ]

    operations = [
        migrations.AddField(
            model_name='item',
            name='date_added',
            field=models.DateTimeField(default=datetime.datetime(2014, 11, 16, 19, 44, 50, 6698), auto_now_add=True),
            preserve_default=False,
        ),
    ]
