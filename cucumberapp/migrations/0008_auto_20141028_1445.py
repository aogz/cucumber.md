# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('cucumberapp', '0007_auto_20141024_1043'),
    ]

    operations = [
        migrations.RenameField(
            model_name='item',
            old_name='price',
            new_name='old_price',
        ),
        migrations.AddField(
            model_name='item',
            name='new',
            field=models.BooleanField(default=1),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='item',
            name='new_price',
            field=models.CharField(max_length=64, null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='item',
            name='on_sale',
            field=models.BooleanField(default=1),
            preserve_default=False,
        ),
    ]
