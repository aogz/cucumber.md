# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('cucumberapp', '0005_slider'),
    ]

    operations = [
        migrations.AddField(
            model_name='slider',
            name='link',
            field=models.CharField(default=1, max_length=255, verbose_name=b'Link on button'),
            preserve_default=False,
        ),
    ]
