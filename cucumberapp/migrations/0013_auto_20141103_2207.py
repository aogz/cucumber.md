# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('cucumberapp', '0012_auto_20141103_2200'),
    ]

    operations = [
        migrations.AlterField(
            model_name='item',
            name='available',
            field=models.CharField(max_length=255),
        ),
    ]
