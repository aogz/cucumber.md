# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import ckeditor.fields


class Migration(migrations.Migration):

    dependencies = [
        ('cucumberapp', '0010_auto_20141103_2050'),
    ]

    operations = [
        migrations.AddField(
            model_name='item',
            name='available',
            field=models.CharField(default=b'order', max_length=255, choices=[(b'in_stock', b'In stock'), (b'order', b'By order'), (b'n/a', b'Not Available')]),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='brand',
            name='description',
            field=ckeditor.fields.RichTextField(),
        ),
    ]
