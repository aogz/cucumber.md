# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import ckeditor.fields


class Migration(migrations.Migration):

    dependencies = [
        ('cucumberapp', '0006_slider_link'),
    ]

    operations = [
        migrations.AddField(
            model_name='item',
            name='camera_back',
            field=models.CharField(default=1, max_length=255, verbose_name=b'\xd0\x97\xd0\xb0\xd0\xb4\xd0\xbd\xd1\x8f\xd1\x8f \xd0\xba\xd0\xb0\xd0\xbc\xd0\xb5\xd1\x80\xd0\xb0'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='item',
            name='camera_front',
            field=models.CharField(default=1, max_length=255, verbose_name=b'\xd0\xa4\xd1\x80\xd0\xbe\xd0\xbd\xd1\x82\xd0\xb0\xd0\xbb\xd1\x8c\xd0\xbd\xd0\xb0\xd1\x8f \xd0\xba\xd0\xb0\xd0\xbc\xd0\xb5\xd1\x80\xd0\xb0'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='item',
            name='dimensions',
            field=models.CharField(default=1, max_length=255, verbose_name=b'\xd0\xa0\xd0\xb0\xd0\xb7\xd0\xbc\xd0\xb5\xd1\x80\xd1\x8b (\xd0\xa8x\xd0\x92x\xd0\xa2)'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='item',
            name='display',
            field=models.CharField(default=1, max_length=255, verbose_name=b'\xd0\xad\xd0\xba\xd1\x80\xd0\xb0\xd0\xbd'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='item',
            name='general',
            field=models.CharField(default=1, max_length=255, verbose_name=b'\xd0\xa1\xd1\x82\xd0\xb0\xd0\xbd\xd0\xb4\xd0\xb0\xd1\x80\xd1\x82'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='item',
            name='memory',
            field=models.CharField(default=1, max_length=255, verbose_name=b'\xd0\x9e\xd0\x97\xd0\xa3'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='item',
            name='numbers_sim',
            field=models.CharField(default=1, max_length=255, verbose_name=b'\xd0\x9a\xd0\xbe\xd0\xbb\xd0\xb8\xd1\x87\xd0\xb5\xd1\x81\xd1\x82\xd0\xb2\xd0\xbe SIM-\xd0\xba\xd0\xb0\xd1\x80\xd1\x82'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='item',
            name='operating_sistem',
            field=models.CharField(default=1, max_length=255, verbose_name=b'\xd0\x9e\xd0\xbf\xd0\xb5\xd1\x80\xd0\xb0\xd1\x86\xd0\xb8\xd0\xbe\xd0\xbd\xd0\xbd\xd0\xb0\xd1\x8f \xd1\x81\xd0\xb8\xd1\x81\xd1\x82\xd0\xb5\xd0\xbc\xd0\xb0'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='item',
            name='processor',
            field=models.CharField(default=1, max_length=255, verbose_name=b'\xd0\x9f\xd1\x80\xd0\xbe\xd1\x86\xd0\xb5\xd1\x81\xd1\x81\xd0\xbe\xd1\x80'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='item',
            name='sim_type',
            field=models.CharField(default=1, max_length=255, verbose_name=b'\xd0\xa2\xd0\xb8\xd0\xbf SIM-\xd0\xba\xd0\xb0\xd1\x80\xd1\x82\xd1\x8b'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='item',
            name='storage',
            field=models.CharField(default=1, max_length=255, verbose_name=b'\xd0\x9e\xd0\xb1\xd1\x8a\xd0\xb5\xd0\xbc \xd0\xbf\xd0\xb0\xd0\xbc\xd1\x8f\xd1\x82\xd0\xb8'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='item',
            name='weight',
            field=models.CharField(default=1, max_length=255, verbose_name=b'\xd0\x92\xd0\xb5\xd1\x81'),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='item',
            name='description',
            field=ckeditor.fields.RichTextField(),
        ),
        migrations.AlterField(
            model_name='page',
            name='body',
            field=ckeditor.fields.RichTextField(),
        ),
        migrations.AlterField(
            model_name='slider',
            name='body',
            field=ckeditor.fields.RichTextField(),
        ),
    ]
