# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import ckeditor.fields


class Migration(migrations.Migration):

    dependencies = [
        ('cucumberapp', '0003_auto_20140908_1935'),
    ]

    operations = [
        migrations.CreateModel(
            name='Page',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=255, verbose_name=b'Title')),
                ('url', models.CharField(unique=True, max_length=128, verbose_name=b'URL')),
                ('body', ckeditor.fields.RichTextField(verbose_name=b'Text')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AlterField(
            model_name='item',
            name='description',
            field=ckeditor.fields.RichTextField(verbose_name=b'Text'),
        ),
    ]
