# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('cucumberapp', '0002_order'),
    ]

    operations = [
        migrations.RenameField(
            model_name='order',
            old_name='title',
            new_name='name',
        ),
    ]
