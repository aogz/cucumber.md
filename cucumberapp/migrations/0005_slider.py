# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import ckeditor.fields


class Migration(migrations.Migration):

    dependencies = [
        ('cucumberapp', '0004_auto_20140908_2008'),
    ]

    operations = [
        migrations.CreateModel(
            name='Slider',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=255, verbose_name=b'Title')),
                ('body', ckeditor.fields.RichTextField(verbose_name=b'Body text')),
                ('button', models.CharField(max_length=255, verbose_name=b'Text on button')),
                ('image', models.ImageField(upload_to=b'slider', blank=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
