# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('cucumberapp', '0008_auto_20141028_1445'),
    ]

    operations = [
        migrations.AddField(
            model_name='order',
            name='more',
            field=models.TextField(default=12),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='item',
            name='new',
            field=models.BooleanField(default=False),
        ),
        migrations.AlterField(
            model_name='item',
            name='on_sale',
            field=models.BooleanField(default=False),
        ),
    ]
