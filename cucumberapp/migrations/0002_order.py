# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('cucumberapp', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Order',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=64)),
                ('phone', models.CharField(max_length=64)),
                ('item', models.CharField(max_length=128)),
                ('status', models.CharField(default=b'pending', max_length=32, choices=[(b'pending', b'waiting approval'), (b'processing', b'processing'), (b'delivered', b'delivered'), (b'declined', b'Declined')])),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
