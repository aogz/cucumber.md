# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Category',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title_ru', models.CharField(max_length=64)),
                ('title_ro', models.CharField(max_length=64)),
                ('parent', models.ForeignKey(blank=True, to='cucumberapp.Category', null=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Item',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=64)),
                ('description', models.TextField(blank=True)),
                ('price', models.CharField(max_length=64, blank=True)),
                ('category', models.ForeignKey(blank=True, to='cucumberapp.Category', null=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='ItemImage',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('file', models.ImageField(upload_to=b'items/%Y/%m/%d', blank=True)),
                ('item', models.ForeignKey(blank=True, to='cucumberapp.Item', null=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
