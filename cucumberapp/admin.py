from django.contrib import admin
from cucumberapp.models import *


# Register your models here.

class CategoryAdmin(admin.ModelAdmin):
    list_display = ('title_ru',)


class ItemImageInline(admin.StackedInline):
    extra = 0
    model = ItemImage


class ItemAdmin(admin.ModelAdmin):
    inlines = [ItemImageInline, ]
    list_display = ('title', 'old_price', 'new_price', 'on_sale', 'new')
    list_editable = ('old_price', 'new_price', 'on_sale', 'new')
    search_fields = ['title']
    exclude = ['short_description']


class OrderAdmin(admin.ModelAdmin):
    list_display = ('name', 'phone', 'item', 'more', 'status')
    list_editable = ('status', )


class BrandAdmin(admin.ModelAdmin):
    list_display = ('title',)


class PageAdmin(admin.ModelAdmin):
    prepopulated_fields = {'url': ('title',)}
    list_display = ('title', )

admin.site.register(Slider)
admin.site.register(Promotion)
admin.site.register(Brand, BrandAdmin)
admin.site.register(Order, OrderAdmin)
admin.site.register(Page, PageAdmin)
admin.site.register(Item, ItemAdmin)
admin.site.register(Category, CategoryAdmin)