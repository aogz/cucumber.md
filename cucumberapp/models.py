# -*- coding: utf-8 -*-

from django.db import models
from ckeditor.fields import RichTextField


ITEM_STATUSES = (
    ('pending', ('waiting approval')),
    ('processing', ('processing')),
    ('delivered', ('delivered')),
    ('declined', ('Declined')),
)


# Create your models here.
class Category(models.Model):
    title_ru = models.CharField(max_length=64)
    title_ro = models.CharField(max_length=64)
    parent = models.ForeignKey('self', null=True, blank=True)

    def __unicode__(self):
        return self.title_ru


class Brand(models.Model):
    title = models.CharField(max_length=255)
    description = RichTextField()
    logo = models.ImageField(upload_to="brands", verbose_name=u"Изображение")

    def __unicode__(self):
        return self.title


class Item(models.Model):
    title = models.CharField(max_length=64)
    description = RichTextField()
    old_price = models.CharField(max_length=64, blank=True)
    new_price = models.CharField(max_length=64, blank=True, null=True)
    category = models.ForeignKey(Category, null=True, blank=True)
    brand = models.ForeignKey(Brand, null=True, blank=True)
    date_added = models.DateTimeField(auto_now_add=True)

    active = models.BooleanField(default=True)
    on_sale = models.BooleanField(default=False)
    new = models.BooleanField(default=False)
    available = models.CharField(max_length=255)

    operating_system = models.CharField(max_length=255, verbose_name="Операционная система")
    storage = models.CharField(max_length=255, verbose_name="Объем памяти")
    processor = models.CharField(max_length=255, verbose_name="Процессор")
    memory = models.CharField(max_length=255, verbose_name="ОЗУ")
    display = models.CharField(max_length=255, verbose_name="Экран")
    camera_front = models.CharField(max_length=255, verbose_name="Фронтальная камера")
    camera_back = models.CharField(max_length=255, verbose_name="Задняя камера")
    numbers_sim = models.CharField(max_length=255, verbose_name="Количество SIM-карт")
    sim_type = models.CharField(max_length=255, verbose_name="Тип SIM-карты")
    weight = models.CharField(max_length=255, verbose_name="Вес")
    dimensions = models.CharField(max_length=255, verbose_name="Размеры (ШxВxТ)")

    def get_main_image(self):
        try:
            img = ItemImage.objects.filter(item=self)[0]
            return img.file
        except:
            return None

    def __unicode__(self):
        return self.title


class ItemImage(models.Model):
    item = models.ForeignKey(Item, null=True, blank=True)
    file = models.ImageField(upload_to='items/%Y/%m/%d', blank=True)


class Order(models.Model):
    name = models.CharField(max_length=64)
    phone = models.CharField(max_length=64)
    item = models.CharField(max_length=128)
    more = models.TextField()
    status = models.CharField(max_length=32, choices=ITEM_STATUSES, default='pending')


class Page(models.Model):
    title = models.CharField(max_length=255, verbose_name="Title")
    url = models.CharField(max_length=128, unique=True, verbose_name='URL')
    body = RichTextField()


class Slider(models.Model):
    title = models.CharField(max_length=255, verbose_name='Title')
    body = RichTextField()
    button = models.CharField(max_length=255, verbose_name='Text on button')
    link = models.CharField(max_length=255, verbose_name='Link on button')
    image = models.ImageField(upload_to='slider', blank=True)


class Promotion(models.Model):
    image = models.ImageField(upload_to='promotion', blank=True)
    link = models.CharField(max_length=255, verbose_name='Link')