from django.core.context_processors import request
 
from cucumberapp.models import *


def categories(request):
    categories = Category.objects.all()    
    return {"categories": categories}


def brands(request):
    brands = Brand.objects.all()
    brand_list = []

    for brand in brands:
        brand_list.append({'brand': brand, 'count': Item.objects.all().filter(brand=brand).count()})

    return {"brands": brand_list}


def slider(request):
    sliders = Slider.objects.all()
    return {"sliders": sliders}


def pages(request):
    pages = Page.objects.all()
    return {"pages": pages}


def promotion(request):
    try:
        prom = Promotion.objects.all()[0]
    except IndexError:
        return {"promotion": ""}

    return {"promotion": prom}